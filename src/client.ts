"use client";

import { Ref, ref } from "@vue/reactivity";
import { ReactElement } from "react";

let popupIdCount = 0;
export const popups = ref([]) as Ref<{
    id: number;
    component: ReactElement;
}[]>;

const zIndexes = {
    veryLow: 500_000,
    low: 1_000_000,
    middle: 2_000_000,
    high: 3_000_000,
};

export function claimZIndex(priority: keyof typeof zIndexes = "low"): number {
    zIndexes[priority] += 100;
    return zIndexes[priority];
}

export async function popup<T extends ReactElement>(
    component: T,
): Promise<{ dispose: () => void }> {
    const id = ++popupIdCount;
    const dispose = () => {
        popups.value = popups.value.filter(popup => popup.id !== id);
    };

    const state = {
        id,
        component,
    };

    popups.value.push(state);

    return { dispose };
}
