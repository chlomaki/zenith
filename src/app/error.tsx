"use client";

import envOption from "@/env";
import ErrorTemplate from "../components/errors/ErrorTemplate";
import { APIError } from "../util/errors/APIError";

export default function Error({
    error,
    reset
}: { 
    error: Error & { digest?: string },
    reset: () => void
}) {
    return (
        <ErrorTemplate code={parseInt(error.message.split(": ")[0])} message={envOption.isDebug ? error.message : "An error has occurred."} />
    )
}