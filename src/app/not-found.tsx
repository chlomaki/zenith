import ErrorTemplate from "../components/errors/ErrorTemplate";

export default function NotFound() {
    return (
        <ErrorTemplate code={404} message="This page was not found." />
    );
}