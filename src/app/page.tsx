import { inter, montserratSemiBold } from "@/fonts";
import LandingTeam from "../components/landing/LandingTeam";
import { MetadataUtil } from "../util/MetadataUtil";

export const metadata = MetadataUtil.getMetadataForPage("Home");

export default function Page() {
    return (
        <div className="flex flex-col items-center max-h-full min-h-full">
            <div className="flex flex-col items-center text-center w-[70%] mt-32 mb-32">
                <div
                    className={`${montserratSemiBold.className} text-5xl txt-col-[80%] mb-6`}
                >
                    The Challenge List
                </div>
                <div className={`${inter.className} txt-col-[60%] text-lg`}>
                    Welcome to the official Geometry Dash Challenge List
                    website! Here you will find a collection of the hardest
                    challenges ever created and achieved. Complete challenges
                    and collect medals on the way to rise to the top!
                </div>
            </div>
            <LandingTeam
                listTeam={[
                    {
                        id: "n",
                        username: "fat Irisu",
                        created_at: 0,
                        updated_at: 0,
                        country_code: "US",
                        subdivision_code: "CA",
                        flags: 0,
                        groups: [],
                    },
                    {
                        id: "n",
                        username: "HUUUGE Irisu",
                        created_at: 0,
                        updated_at: 0,
                        country_code: "US",
                        subdivision_code: "CA",
                        flags: 0,
                        groups: [],
                    },
                    {
                        id: "n",
                        username: "GIRTH Irisu",
                        created_at: 0,
                        updated_at: 0,
                        country_code: "US",
                        subdivision_code: "CA",
                        flags: 0,
                        groups: [],
                    },
                ]}
                developmentTeam={[
                    {
                        id: "n",
                        username: "Evil Irisu",
                        created_at: 0,
                        updated_at: 0,
                        country_code: "US",
                        subdivision_code: "CA",
                        flags: 0,
                        groups: [],
                    },
                ]}
            />
        </div>
    );
}
