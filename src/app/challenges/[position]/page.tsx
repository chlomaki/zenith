import { ChevronLeft } from "@mui/icons-material";
import Link from "next/link";
import { cache } from "react";
import { api } from "@/api/api";
import { Challenge } from "@/api/challenge";
import User from "@/components/User";
import { montserrat } from "@/fonts";
import { assertOk } from "@/util/Assert";
import { MetadataUtil } from "@/util/MetadataUtil";
import "./page.scss";

interface PageParams {
    params: {
        position: string
    }
}

const getChallenge = cache(async (position: string) => {
    const challenge = await api.challenge.fetch.position(undefined, { position });

    assertOk(challenge);
    return challenge.val.data;
});

export async function generateMetadata({ params }: PageParams) {
    const challenge = await getChallenge(params.position);

    return MetadataUtil.getMetadataForPage(`Challenge ${challenge.position}`);
}

export default async function Page({ params }: PageParams) {
    const challenge = await getChallenge(params.position);

    return (
        <div className="flex flex-col gap-4">
            <Link href="/">
                <div className="flex flex-row gap-2 txt-col-[40%]">
                    <ChevronLeft />
                    <p>Back to list</p>
                </div>
            </Link>
            <div className="flex flex-row gap-4">
                <PeoplesComponent challenge={challenge} />
                <div className="flex flex-col gap-8 bg-col-[9%] grow py-6 px-10 h-fit rounded-md shadow-md">
                    <LevelInfo challenge={challenge} />
                    <LevelRecords challenge={challenge} />
                </div>
            </div>
        </div>
    );
}

export function PeoplesComponent({ challenge }: { challenge: Challenge }) {
    return <div className="flex flex-col gap-4 bg-col-[9%] min-w-60 w-60 p-4 h-fit rounded-md shadow-md">
        {challenge.publisher && <div className="flex flex-col gap-2">
            <p>Published by</p>
            <User user={challenge.publisher} />
        </div>}
        {challenge.creators && challenge.creators.length > 0 && <div className="flex flex-col gap-2">
            <p>Created by</p>
            {challenge.creators.map((creator) => <User user={creator} key={creator.id} />)}
        </div>}
        {challenge.verifier && <div className="flex flex-col gap-2">
            <p>Verified by</p>
            <User user={challenge.verifier} />
        </div>}
    </div>;
}

export function LevelInfo({ challenge }: { challenge: Challenge }) {
    // Don't look at me look at the person who made this regex
    const embedId = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube(?:-nocookie)?\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|live\/|v\/)?)([\w\-]+)(\S+)?$/g.exec(challenge.video)![5];

    return <div className="flex flex-row gap-6">
        <div className="flex flex-col grow justify-between">
            <div className="flex flex-col gap-1 grow">
                <h1 className={`${montserrat.className} txt-col-[90%] text-xl`}>{challenge.name}</h1>
                <p className="txt-col-[60%]">challenge description here (taken from gd servers or something idk)</p>
            </div>
            <div className="flex flex-row gap-4">
                {challenge.level_id && <div className="flex flex-col gap-1">
                    <p className="txt-col-[60%]">Level ID</p>
                    <p>{challenge.level_id}</p>
                </div>}
                <div className="flex flex-col gap-1">
                    <p className="txt-col-[60%]">FPS</p>
                    <p>Any</p>
                </div>
            </div>
        </div>
        <iframe
            src={`https://www.youtube.com/embed/${embedId}`}
            width="356"
            height="200"
            className="rounded-md"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            title="Embedded youtube"
        />
    </div>;
}

export function LevelRecords({ challenge }: { challenge: Challenge }) {
    const dateFormatter = new Intl.DateTimeFormat("en-us", { dateStyle: "medium" });

    return <table>
        <thead>
            <tr>
                <th className="px-4 py-2 font-normal text-left text-sm w-max txt-col-[40%]">Player</th>
                <th className="px-2 font-normal text-right text-sm w-28 txt-col-[40%]">Achieved on</th>
            </tr>
        </thead>
        <tbody>
            {/* todo : we'll probably have to filter this? */}
            {challenge.records && challenge.records.map((record) => <tr key={record.id}>
                <th className="px-4 py-2 rounded-l font-normal text-left text-sm w-max txt-col-[80%]">{record.player.username}</th>
                {/* todo : the date is wrong (on our side) */}
                <th className="p-2 font-normal text-right text-sm w-28 txt-col-[80%]">{dateFormatter.format(new Date().setUTCMilliseconds(record.submitted_at))}</th>
                <th className="px-4 py-2 rounded-r font-normal text-right text-sm w-20 txt-col-[60%]"><Link href={record.video} target="_blank">Youtube</Link></th>
            </tr>)}
        </tbody>
    </table>;
}
