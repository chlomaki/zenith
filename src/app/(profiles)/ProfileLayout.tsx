// note : while yes you can convert this to a standard component, it's not recommended to do so.
//        this is strictly for the purpose of providing a layout for the profile pages.
//        whether it be for your own profile or someone else's profile.
//
//        currently it is not possible to tell the difference between the two (or at least i don't know how),
//        so it's best to leave it as is.

import { Account } from "@/api/account";
import { getBaseUrl } from "@/api/api";
import { AccountInfo } from "@/components/profile/AccountInfo";
import { ProfilePages } from "@/components/profile/ProfilePages";

export function ProfileLayout({ children, account }: { children: React.ReactNode, account: Account }) {
    return <>
        <div className="absolute inset-0 max-h-[64vh] overflow-hidden -z-10">
            <img className="w-full h-full object-cover object-bottom sticky top-0" src={getBaseUrl() + `/assets/banners/${account.banner_hash}`} alt="" />
            <div className="absolute inset-0 z-10" style={{
                background: "linear-gradient(to bottom, hsla(var(--base-hue), var(--base-saturation), 7%, 70%) 0%, hsla(var(--base-hue), var(--base-saturation), 7%, 100%) 100%)"
            }} />
        </div>
        <div className="flex flex-row gap-4 pt-16 mb-8">
            <div className="flex flex-col gap-2 max-w-80 w-full">
                <AccountInfo account={account} />
                <ProfilePages account={account} />
            </div>
            <div className="grow h-fit rounded-md bg-col-[9%] shadow-sm px-10 py-6">
                {children}
            </div>
        </div>
    </>;
}