import { cache } from "react";
import { api } from "@/api/api";
import { ProfileLayout } from "@/app/(profiles)/ProfileLayout";
import { ProfileInfo } from "@/components/profile/ProfileInfo";
import { assertOk } from "@/util/Assert";
import { MetadataUtil } from "@/util/MetadataUtil";

interface PageParams {
    params: {
        id: string
    }
}

const getAccount = cache(async (id: string) => {
    const account = await api.account.fetch.byId(undefined, { id });
    assertOk(account);
    return account.val.data;
});

const getProfile = cache(async (id: string) => {
    const account = await api.player.fetch.profileById(undefined, { id });
    assertOk(account);
    return account.val.data;
});

export async function generateMetadata({ params }: PageParams) {
    const profile = await getAccount(params.id);
    return MetadataUtil.getMetadataForPage(`Profile ${profile.username}`);
}

export default async function Page({ params }: PageParams) {
    const account = await getAccount(params.id);
    const profile = await getProfile("1"); // todo : get profile from account if it exists

    return <ProfileLayout account={account}>
        {profile ?
            <ProfileInfo profile={profile} />
            // todo : should be the opposite way.
            : <p>This account does not have a profile linked.</p>}
    </ProfileLayout>;
}