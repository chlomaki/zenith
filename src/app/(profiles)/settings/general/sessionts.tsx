"use client";

import { mdiMonitor } from "@mdi/js";
import Icon from "@mdi/react";
import { self } from "@/account";
import { PublicSession } from "@/api/account";
import { interSemiBold } from "@/fonts";

export function Sessions({ sessions }: { sessions: PublicSession[] }) {
    sessions = sessions.filter((session) => session.id !== self!.tokenId);

    const formatter = new Intl.RelativeTimeFormat("en", { numeric: "auto" });

    return <div className="flex flex-col gap-2">
        <div className="flex flex-row justify-between bg-col-[12%] p-4 rounded-md shadow-sm">
            <div className="flex flex-col gap-2">
                <div className="flex flex-row gap-1">
                    <div className="icon">
                        <Icon size={1} path={mdiMonitor} />
                    </div>
                    <p>Desktop</p>
                </div>
                <p className="txt-col-[40%]">Current session</p>
            </div>
            <div className="flex flex-col gap-2 txt-col-[40%] items-end">
                <div className="text-sm flex flex-row gap-1">Last active: <p className={interSemiBold.className}>Now</p></div>
                <p>Canada (192.168.0.1)</p>
            </div>
        </div>
        <div className="flex flex-col gap-2 bg-col-[7%] p-4 rounded-md">
            {sessions.map((session) => (
                <div key={session.id} className="flex flex-row justify-between bg-col-[9%] p-4 rounded-md shadow-sm">
                    <div className="flex flex-col gap-2">
                        <div className="flex flex-row gap-1">
                            <div className="icon">
                                <Icon size={1} path={mdiMonitor} />
                            </div>
                            <p>Desktop</p>
                        </div>
                        <p className="txt-acc-[70%] cursor-pointer">End session</p>
                    </div>
                    <div className="flex flex-col gap-2 txt-col-[40%] items-end">
                        <div className="text-sm flex flex-row gap-1">Last active: <p className={interSemiBold.className}>{formatter.format(-(Math.floor(Date.now() / 1000) - (session.updated_at ?? session.created_at)), "seconds")}</p></div>
                        <p>Canada (192.168.0.1)</p>
                    </div>
                </div>
            ))}
        </div>
    </div>;
}
