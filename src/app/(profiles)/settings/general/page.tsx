import { cache } from "react";
import { api } from "@/api/api";
import { ClientSettings } from "@/app/(profiles)/settings/general/clientSettings";
import { Sessions } from "@/app/(profiles)/settings/general/sessionts";
import { montserratSemiBold } from "@/fonts";
import { assertOk } from "@/util/Assert";

// todo : temporary hack
const getSessions = cache(async () => {
    const account = await api.account.authorization.getSessions();
    assertOk(account);
    return account.val.data;
});

export default async function Page() {
    const sessions = await getSessions();
    
    return <>
        <h1 className={`${montserratSemiBold.className} txt-col-[95%] text-xl mb-4`}>Client Settings</h1>
        <ClientSettings />
        <h1 className={`${montserratSemiBold.className} txt-col-[95%] text-xl mb-2`}>Sessions</h1>
        <p className="txt-col-[40%] mb-4">All logged in sessions of your Challenge List Account.</p>
        <Sessions sessions={sessions} />
    </>;
}