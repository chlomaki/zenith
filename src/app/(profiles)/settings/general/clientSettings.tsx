"use client";

import { useState } from "react";
import { Select, SelectContent, SelectGroup, SelectItem, SelectTrigger, SelectValue } from "@/components/Select";
import { SettingItem } from "@/components/settings/SettingItem";
import { Switch } from "@/components/Switch";
import { localStorage } from "@/localStorage";
import { languages } from "@/settings/languages";

export function ClientSettings() {
    // todo : pullstate?
    const [language, setLanguage] = useState(localStorage.getItem("lang") ?? "en");
    const [reducedMotion, setReducedMotion] = useState((localStorage.getItem("reducedMotion") ?? "false") === "true");
    const [useSystemFont, setSseSystemFont] = useState((localStorage.getItem("useSystemFont") ?? "false") === "true");

    return <div className="flex flex-col gap-2 mb-6">
        <SettingItem label="Language" description="The Challenge List is being translated into various languages by volunteers. You can help at Crowdin.">
            <Select
                defaultValue="en"
                value={language}
                onValueChange={(value) => {
                    setLanguage(value);
                    localStorage.setItem("lang", value);
                }}
            >
                <SelectTrigger>
                    <SelectValue />
                </SelectTrigger>
                <SelectContent>
                    <SelectGroup>
                        {languages.map((lang) => (
                            <SelectItem key={lang.key} value={lang.key}>
                                {lang.label}
                            </SelectItem>
                        ))}
                    </SelectGroup>
                </SelectContent>
            </Select>
        </SettingItem>
        <SettingItem label="Reduce UI Animations">
            <Switch checked={reducedMotion} onCheckedChange={(val) => {
                setReducedMotion(val);
                localStorage.setItem("reducedMotion", val ? "true" : "false");
            }} />
        </SettingItem>
        <SettingItem label="Use System Font">
            <Switch checked={useSystemFont} onCheckedChange={(val) => {
                setSseSystemFont(val);
                localStorage.setItem("useSystemFont", val ? "true" : "false");
            }} />
        </SettingItem>
    </div>;
}