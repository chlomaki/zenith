"use client";

import { self } from "@/account";
import { ProfileLayout } from "@/app/(profiles)/ProfileLayout";

export default function SelfProfileLayout({ children }: { children: React.ReactNode }) {
    return <ProfileLayout account={self!}>
        {children}
    </ProfileLayout>;
}