import { cookies } from "next/headers";
import { redirect } from "next/navigation";
import SelfProfileLayout from "@/app/(profiles)/settings/SelfProfileLayout";

export default function SettingsLayout({ children }: { children: React.ReactNode }) {
    if (cookies().get("session") === undefined)
        return redirect("/");
    
    return <SelfProfileLayout>
        {children}
    </SelfProfileLayout>;
}