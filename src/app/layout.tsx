import "../styles/app.scss";

import { PopupsContainer } from "@/app/popupsContainer";
import { inter } from "@/fonts";
import ClientOnly from "../components/ClientOnly";
import Navbar from "../components/navbar/Navbar";

import "@/account";

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
            <body className={`${inter.className} overflow-x-hidden`}>
                <div id="app">
                    <Navbar />
                    <ClientOnly>
                        { /* todo : remove pointer-events when there are popups */}
                        <div className="absolute inset-0 pointer-events-none" style={{ zIndex: 3_000_000 }}>
                            <PopupsContainer />
                        </div>
                    </ClientOnly>
                    <div className="flex flex-col h-screen">
                        <div className="px-4 grow">
                            <div className="mt-28 mx-auto max-w-7xl w-full">
                                {children}
                            </div>
                        </div>
                        <div className="relative flex items-end h-64 w-full px-4">
                            <div className="absolute inset-0 overflow-hidden" style={{ background: "linear-gradient(to top, rgba(6,7,10,0.25) 0%, rgba(6,7,10,0) 100%)" }}>
                                <div className="absolute w-[3000px] h-[3000px] right-[-1800px] top-[32px] rounded-full border-2 bord-col-[9%]" />
                                <div className="absolute w-[5000px] h-[5000px] right-[-2300px] top-[94px] rounded-full border-2 bord-col-[9%]" />
                            </div>
                            <div className="text-xs z-10 flex flex-row gap-8 justify-between mb-16 mx-auto max-w-7xl w-full txt-col-[30%]">
                                <p>© 2024 Challenge List. All rights reserved.</p>
                                <p>Powered by Apotheosis (v1.0.0) and Zenith (v1.0.0)</p>
                                <div className="flex flex-row gap-4">
                                    <a href="#">Terms and Conditions</a>
                                    <a href="#">Privacy Policy</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    );
}
