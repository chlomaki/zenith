"use client";

import { popups } from "@/client";
import { effect, stop } from "@vue/reactivity";
import { ReactElement, ReactNode, useEffect, useState } from "react";

export function PopupsContainer() {  
    const [components, setComponents] = useState<ReactElement[]>([]);
    
    useEffect(() => {
        const runner = effect(() => {
            setComponents(popups.value.map(p => p.component));
        });

        return () => stop(runner);
    }, [])
    
    return <>{components.map((c, i) => <div key={i}>{c}</div>)}</>
}
