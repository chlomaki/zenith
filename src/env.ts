const envOption = {
    isDebug: process.env.NODE_ENV === 'development',
    verbose: false,
    quiet: false
}

for (const key of Object.keys(envOption) as (keyof typeof envOption)[]) {
    if (process.env["ZN_" + key.replace(/[A-Z]/g, (x) => "_" + x.toUpperCase())]) {
        envOption[key] = true;
    }
}

export default envOption;
