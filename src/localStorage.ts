"use client";

type Keys =
    "account" |
    "lang" |
    "fontSize" |
    "reducedMotion" |
    "useSystemFont";

// todo : figure out why the fuck the server is touching this code.
//        and why it's throwing an error even though we're EXPLICITLY
//        checking to see if the window is undefined.
export const localStorage = {
    getItem: (key: Keys): string | null => {
        if (window === undefined) return null;
        return window.localStorage.getItem(key);
    },
    setItem: (key: Keys, value: string): void => {
        if (window === undefined) return;
        window.localStorage.setItem(key, value);
    },
    removeItem: (key: Keys): void => {
        if (window === undefined) return;
        window.localStorage.removeItem(key);
    },
    getItemAsJson: <T = any>(key: Keys): T | undefined => {
        if (window === undefined) return undefined;
        const item = localStorage.getItem(key);
        if (item === null) return undefined;
        return JSON.parse(item) as T;
    },
    setItemAsJson: <T = any>(key: Keys, value: T): void => {
        if (window === undefined) return;
        localStorage.setItem(key, JSON.stringify(value));
    },
};