// todo : add more languages through i18n

import envOption from "@/env";

export let languages = [
    { key: "en", label: "English" },
    { key: "jp", label: "日本語" },
];

if (envOption.isDebug) {
    // todo : expose this for users who want to translate the app
    languages = [...languages, { key: "debug", label: "Debug" }];
}
