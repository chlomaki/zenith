import envOption from "@/env";

export class Logger {
    constructor(private name: string) { }

    private log(icon: string, message: string) {
        if (!envOption.quiet)
            console.log(`${icon} [${this.name}] ${message}`);
    }

    public info(message: string) {
        this.log('⬤', message);
    }

    public warn(message: string) {
        this.log('▲', message);
    }

    public error(message: string) {
        this.log('✖', message);
    }

    public debug(message: string) {
        if (envOption.isDebug)
            this.log('▷', message);
    }
}