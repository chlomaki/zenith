import { Ok, Result } from "ts-results";
import { APIError } from "./errors/APIError";
import { HTTPError } from "@/api/router";

export function assertOk<T, E>(value: Result<T, E>, message?: string): asserts value is Ok<T> {
    if (value.err) {
        if (typeof value.val === "object" && "code" in value.val!) {
            throw new APIError(value.val as unknown as HTTPError);
        }

        throw new Error(message ?? (value.val as Error).toString());
    }
}