import { HTTPError } from "../../api/router";

export class APIError extends Error {
    public error: HTTPError;

    constructor(error: HTTPError) {
        super(`${error.code}: ${error.message}`);

        this.error = error;
    }
}