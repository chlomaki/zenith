"use client";

import { mdiAccount, mdiAccountCircle, mdiCogs } from "@mdi/js";
import Icon from "@mdi/react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { self } from "@/account";
import { Account } from "@/api/account";
import { interSemiBold } from "@/fonts";

export function ProfilePages({ account }: { account: Account }) {
    const pathname = usePathname();
    const isProfile = pathname.startsWith("/profiles/");
    // const isRecords = pathname.startsWith("/records");
    const isSettings = pathname.startsWith("/settings/general");
    const isAccount = pathname.startsWith("/settings/account");

    if (!self || self.id !== account.id) return <></>;

    return <div className="flex flex-col gap-2 bg-col-[9%] rounded-md shadow-md p-4">
        <PageButton active={isProfile} label="Profile" href={`/profiles/${account.id}`} icon={<Icon path={mdiAccountCircle} size={1} />} />
        {/* <PageButton label="Records" href={`/profiles/${account.id}/records`} icon={<Icon path={mdiAccount} size={1} />} /> */}
        <PageButton active={isSettings} label="General" href="/settings/general" icon={<Icon path={mdiCogs} size={1} />} />
        <PageButton active={isAccount} label="Account" href="/settings/account" icon={<Icon path={mdiAccount} size={1} />} />
    </div>;
}

function PageButton({ active, label, href, icon }: { active: boolean,label: string, href: string, icon: React.ReactNode }) {
    return <Link href={href} className={`flex flex-row gap-2 items-center rounded-md px-4 py-2 ${active ? "bg-col-[12%]" : "txt-col-[60%]"}`}>
        <div className="icon">{icon}</div>
        <p className={`${active ? interSemiBold.className : ""}`}>{label}</p>
    </Link>;
}
