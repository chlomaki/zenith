import { Group } from "@/api/group";
import { GroupChip } from "@/components/profile/GroupChip";

export function GroupChipArray({ groups }: { groups: Group[] }) {
    groups = groups.filter(g => g.id !== "default"); // remove default group

    if (groups.length === 0) {
        // todo : probably better to render with the appropriate height just to keep
        //        a consistent layout elsewhere.
        return <></>;
    }

    const primaryGroup = groups.reduce((a, b) => a.priority > b.priority ? a : b);
    const otherGroups = groups.filter(g => g.id !== primaryGroup.id);

    return <div className="flex flex-row gap-1">
        <GroupChip group={primaryGroup} />
        {otherGroups.map(g => <GroupChip key={g.id} group={g} iconOnly />)}
    </div>;
}