import { Account } from "@/api/account";
import { AccountCard } from "@/components/profile/AccountCard";
import styles from "./AccountInfo.module.scss";

export function AccountInfo({ account }: { account: Account }) {
    return <div className="w-full h-fit rounded-md bg-col-[9%] shadow-sm p-5 flex flex-col gap-3">
        <div style={{
            marginTop: "-4.75rem",
        }}>
            <AccountCard account={account} background={false} centered={false} />
        </div>
        <p className="text-sm txt-col-[60%]">
            biography here... the user can choose any length of bio that they desire, but it’s encouraged to keep it sweet, simple, and short.
        </p>
        <span className="w-full h-[3px] rounded-full bg-col-[12%]" />
        <div className="flex flex-col gap-2">
            <div className="flex flex-row justify-between text-sm">
                <p>Challenges completed</p>
                {/* todo : get completed challenges from API */}
                <p className="txt-col-[60%]">73</p>
            </div>
            {/* todo : implement packs using API */}
            <div className="flex flex-row justify-between text-sm">
                <p>Packs completed</p>
                <div className="flex flex-row gap-1">
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={styles["light"]} />
                    <span className={`${styles["light"]} ${styles["light-active"]}`} />
                    <span className={`${styles["light"]} ${styles["light-active"]}`} />
                    <span className={`${styles["light"]} ${styles["light-active"]}`} />
                    <span className={`${styles["light"]} ${styles["light-active"]}`} />
                </div>
            </div>
        </div>
    </div>;
}