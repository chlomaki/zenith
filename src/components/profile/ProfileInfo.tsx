import Link from "next/link";
import { PlayerProfile } from "@/api/player";
import { MiniChallengeCard } from "@/components/challenge/MiniChallengeCard";
import { montserrat, montserratSemiBold } from "@/fonts";

export async function ProfileInfo({ profile }: { profile: PlayerProfile }) {
    return <div className="flex flex-col gap-8">
        {profile.completions && profile.completions.length > 0 && <ProfileSection title="Completed Challenges" count={profile.completions?.length ?? 0}>
            {profile.completions?.map((record) => <MiniChallengeCard key={record.id} challenge={record.challenge}>
                <div className="txt-col-[60%]">
                    <Link href={record.video} target="_blank">Youtube</Link>
                </div>
            </MiniChallengeCard>)}
        </ProfileSection>}
        {profile.verifications && profile.verifications.length > 0 && <ProfileSection title="Completed Challenges" count={profile.verifications?.length ?? 0}>
            {profile.verifications?.map((record) => <MiniChallengeCard key={record.id} challenge={record.challenge}>
                <div className="txt-col-[60%]">
                    <Link href={record.video} target="_blank">Youtube</Link>
                </div>
            </MiniChallengeCard>)}
        </ProfileSection>}
    </div>;
}

export function ProfileSection({ title, count, children }: {
    title: string;
    count?: number;
    children: React.ReactNode;
}) {
    return <div className="flex flex-col gap-2">
        <div className="flex flex-row gap-2">
            <h1 className={`${montserratSemiBold.className} txt-acc-[90%]`}>{title}</h1>
            <h2 className={`${montserratSemiBold.className} txt-col-[20%]`}>•</h2>
            {count ? <h2 className={`${montserrat.className} txt-col-[40%]`}>{count}</h2> : <></>}
        </div>
        {children}
    </div>;
}
