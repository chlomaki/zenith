import { Account } from "@/api/account";
import { getBaseUrl } from "@/api/api";
import { GroupChipArray } from "@/components/profile/GroupChipArray";
import { Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from "@/components/Tooltip";
import Twemoji from "@/components/Twemoji";
import { getCountry } from "@/util/countries";

interface AccountCardProps {
    account: Account;
    background?: boolean;
    centered?: boolean;
}

export function AccountCard({ account, background = true, centered = true }: AccountCardProps) {
    const flag = getCountry(account.country_code ?? "XX");

    return <div className={`flex flex-row gap-2 ${background ? "bg-col-[12%] rounded-md p-2" : ""} ${centered ? "items-center" : ""}`}>
        <div className="w-16 h-16">
            <img className="rounded-md overflow-hidden w-full h-full object-cover" src={getBaseUrl() + `/assets/avatars/${account.avatar_hash}`} alt="" />
        </div>
        <div className="flex flex-col gap-1">
            <div className="flex flex-row gap-2 items-center">
                <p className="txt-col-[95%]">{account.username}</p>
                <TooltipProvider>
                    <Tooltip>
                        <TooltipTrigger asChild>
                            <Twemoji emoji={flag?.emoji ?? ""} />
                        </TooltipTrigger>
                        <TooltipContent>
                            {flag ? <div className="p-2 bg-col-[12%] text-sm rounded-md shadow-md">
                                <p>{flag.name} ({account.subdivision_code})</p>
                            </div> : <></>}
                        </TooltipContent>
                    </Tooltip>
                </TooltipProvider>
            </div>
            <div className="text-xs">
                <GroupChipArray groups={account.groups} />
            </div>
        </div>
    </div>;
}