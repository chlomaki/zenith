import { mdiXml } from "@mdi/js";
import Icon from "@mdi/react";
import { TooltipContent, TooltipTrigger } from "@radix-ui/react-tooltip";
import { Group } from "@/api/group";
import { Tooltip, TooltipProvider } from "@/components/Tooltip";

export function GroupChip({ group, iconOnly }: { group: Group, iconOnly?: boolean }) {
    if (group.id === "default") {
        return <></>;
    }

    return <TooltipProvider>
        <Tooltip>
            <TooltipTrigger asChild>
                <div className="flex flex-row w-fit h-fit gap-1 px-2 py-0.5 rounded-md" style={{
                    background: `#${group.color}`,
                    color: "white"
                }}>
                    <div className="icon">
                        <Icon path={mdiXml} size={1} />
                    </div>
                    {!iconOnly && <p>{group.name}</p>}
                </div>
            </TooltipTrigger>
            <TooltipContent>
                {iconOnly && <div className="p-2 bg-col-[12%] text-sm rounded-md shadow-md">
                    <p>{group.name}</p>
                </div>}
            </TooltipContent>
        </Tooltip>
    </TooltipProvider>;

    // return <Tooltip content={
    //     <>
    //         {iconOnly && <div className="p-2 bg-col-[12%] text-sm rounded-md shadow-md">
    //             <p>{group.name}</p>
    //         </div>}
    //     </>
    // }>
    //     <div className="flex flex-row w-fit h-fit gap-1 px-2 py-0.5 rounded-md" style={{
    //         background: `#${group.color}`,
    //         color: "white"
    //     }}>
    //         <div className="icon">
    //             <Icon path={mdiXml} size={1} />
    //         </div>
    //         {!iconOnly && <p>{group.name}</p>}
    //     </div>
    // </Tooltip>;
}