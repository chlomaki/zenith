import { Account } from '@/api/account';
import { Player } from '@/api/challenge';
import Image from 'next/image';

interface UserProps {
    user: Account | Player
}

export default function User({ user }: UserProps) {
    // todo : link account if exists
    return <div className="flex flex-row items-center gap-2">
        {/* <Image className="rounded-full" src={`/${user.id}`} alt="Avatar" width={32} height={32} /> */}
        <Image className="rounded-full" src="https://cdn.discordapp.com/avatars/1103662691068952596/16956a2d419a0bba4bd37b93c7800e00.webp?size=128" alt="Avatar" width={32} height={32} /> { /* todo : cdn server */ }
        <div className="txt-col-[60%]">{user.username}</div>
    </div>
}