import { ChallengeMinified } from "@/api/challenge";
import { montserratSemiBold } from "@/fonts";

export function MiniChallengeCard({ challenge, children }: { challenge: ChallengeMinified, children?: React.ReactNode }) {
    let bgColor = "hsl(var(--base-hue), var(--base-saturation), 16%)";
    let txtColor = "hsl(var(--base-hue), var(--base-saturation), 80%)";

    switch (challenge.position) {
    case 1:
        bgColor = "#FFB133";
        txtColor = "black";
        break;
    case 2:
        bgColor = "#999999";
        txtColor = "black";
        break;
    case 3:
        bgColor = "#FF5C33";
        txtColor = "black";
        break;
    }

    return <div className="flex flex-row rounded-md w-full h-8 bg-col-[12%]">
        <div className="rounded-md flex justify-center items-center h-full w-24"
            style={{ background: bgColor }}
        >
            <h1 className={montserratSemiBold.className}
                style={{ color: txtColor }}
            >#{challenge.position}</h1>
        </div>
        <div className="z-10 relative -ml-2 rounded-md bg-col-[12%] h-full grow">
            <div className="-z-10 absolute inset-0 w-1/2 overflow-hidden">
                <img className="object-cover h-full w-full rounded-md" src="https://pbs.twimg.com/media/GRUQzpxbYAA9_8x?format=jpg&name=orig" alt="" />
                <div className="absolute inset-0 z-10 rounded-l-md" style={{
                    background: "linear-gradient(to right, hsla(var(--base-hue), var(--base-saturation), 12%, 70%) 0%, hsla(var(--base-hue), var(--base-saturation), 12%, 100%) 95%)"
                }} />
            </div>
            <div className="flex flex-row justify-between items-center h-full px-4">
                <div className="flex flex-row gap-1 txt-col-[90%]">
                    <p>{challenge.name}</p>
                    <p className="opacity-25">•</p>
                    <p>{challenge.publisher?.username ?? "Unknown publisher"}</p>
                </div>
                {children}
            </div>
        </div>
    </div>;
}