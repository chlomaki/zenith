import twemoji from "@discordapp/twemoji";
import React, { memo } from "react";

function Twemoji({ emoji }: { emoji: string }) {
    return <span
        className="inline-block w-5 h-5"
        dangerouslySetInnerHTML={{
            __html: twemoji.parse(emoji, {
                folder: "svg",
                ext: ".svg"
            })
        }}
    />;
}

export default memo(Twemoji);