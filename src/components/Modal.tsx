"use client";

import { claimZIndex } from "@/client";
import { ref } from "@vue/reactivity";
import { RefObject, useEffect, useRef } from "react";

type AnchorX = "left" | "center" | "right";
type AnchorY = "top" | "middle" | "bottom";

interface ModalProps {
    children: React.ReactNode,
    anchor?: { x: AnchorX; y: AnchorY; }
    src?: RefObject<HTMLElement> | null;
    zPriority?: "low" | "middle" | "high";
    transparentBg?: boolean;
    noOverlap?: boolean;
    onOpen?: () => void;
    onClose?: () => void;
}

export default function Modal(props: ModalProps) {
    const children = props.children;
    const anchor = props.anchor ?? { x: "center", y: "middle" };
    const src = props.src?.current ?? null;
    const zPriority = props.zPriority ?? "low";
    const transparentBg = props.transparentBg ?? false;
    const noOverlap = props.noOverlap ?? true;
    const onOpen = props.onOpen ?? (() => { });
    const onClose = props.onClose ?? (() => { });

    const maxHeight = useRef<number>();
    const fixed = useRef(false);
    const showing = useRef(true);
    const content = useRef<HTMLDivElement>(null);
    const zIndex = claimZIndex(zPriority);

    const mounted = ref(false);

    let contentClicking = false;

    useEffect(() => {
        if (!mounted.value) {
            window.addEventListener("resize", align);
            
            if (content.current == null) return;
            const el = content.current.children[0];

            el.addEventListener("mousedown", ev => {
                contentClicking = true;

                window.addEventListener("mouseup", ev => {
                    window.setTimeout(() => {
                        contentClicking = false;
                    })
                }, { passive: true, once: true })
            }, { passive: true })

            onOpen();
            mounted.value = true;
        }
    }, []);

    const alignObserver = new ResizeObserver((entries, observer) => {
        align();
    })

    useEffect(() => {
        if (src)
            src.style.pointerEvents = "none";

        alignObserver.observe(content.current!);

        return () => {
            alignObserver.disconnect();
        }
    }, [src])

    function onBgClick() {
        if (contentClicking) return;
        
        if (src)
            src.style.pointerEvents = "auto";
        
        showing.current = false;
        onClose();
    }

    function align() {
        if (src == null) return;
        if (content.current == null) return;

        const srcRect = src.getBoundingClientRect();

        const width = content.current!.offsetWidth;
        const height = content.current!.offsetHeight;

        let left = 0;
        let top = 0

        const MARGIN = 16;
        const SCROLLBAR_THICKNESS = 16;

        const x = srcRect.left + (fixed.current ? 0 : window.scrollX);
        const y = srcRect.top + (fixed.current ? 0 : window.scrollY);

        if (anchor.x === 'center') {
            left = x + (src.offsetWidth / 2) - (width / 2);
        } else if (anchor.x === 'left') {
            left = srcRect.left - (width - src.offsetWidth);
        } else if (anchor.x === 'right') {
            left = x + src.offsetWidth;
        }

        if (anchor.y === 'middle') {
            top = (y - (height / 2));
        } else if (anchor.y === 'top') {
            top = srcRect.top - height;
        } else if (anchor.y === 'bottom') {
            top = y + src.offsetHeight;
        }

        if (fixed.current) {
            if (left + width > (window.innerWidth - SCROLLBAR_THICKNESS)) {
                left = (window.innerWidth - SCROLLBAR_THICKNESS) - width;
            }

            const underSpace = ((window.innerHeight - SCROLLBAR_THICKNESS) - MARGIN) - top;
            const upperSpace = (srcRect.top - MARGIN);

            if (top + height > ((window.innerHeight - SCROLLBAR_THICKNESS) - MARGIN)) {
                if (noOverlap && anchor.x === 'center') {
                    if (underSpace >= (upperSpace / 3)) {
                        maxHeight.current = underSpace;
                    } else {
                        maxHeight.current = upperSpace;
                        top = (upperSpace + MARGIN) - height;
                    }
                } else {
                    top = ((window.innerHeight - SCROLLBAR_THICKNESS) - MARGIN) - height;
                }
            } else {
                maxHeight.current = underSpace;
            }
        } else {
            if (left + width - window.scrollX > (window.innerWidth - SCROLLBAR_THICKNESS)) {
                left = (window.innerWidth - SCROLLBAR_THICKNESS) - width + window.scrollX - 1;
            }

            const underSpace = ((window.innerHeight - SCROLLBAR_THICKNESS) - MARGIN) - (top - window.scrollY);
            const upperSpace = (srcRect.top - MARGIN);

            if (top + height - window.scrollY > ((window.innerHeight - SCROLLBAR_THICKNESS) - MARGIN)) {
                if (noOverlap && anchor.x === 'center') {
                    if (underSpace >= (upperSpace / 3)) {
                        maxHeight.current = underSpace;
                    } else {
                        maxHeight.current = upperSpace;
                        top = window.scrollY + ((upperSpace + MARGIN) - height);
                    }
                } else {
                    top = ((window.innerHeight - SCROLLBAR_THICKNESS) - MARGIN) - height + window.scrollY - 1;
                }
            } else {
                maxHeight.current = underSpace;
            }
        }

        if (top < 0) top = MARGIN;
        if (left < 0) left = 0;

        content.current.style.left = `${left}px`;
        content.current.style.top = `${top}px`;
    }

    return (
        <div
            className="absolute inset-0"
            style={{
                zIndex,
                pointerEvents: showing.current ? "auto" : "none",
                display: showing.current ? "block" : "none",
            }}
            onClick={onBgClick}
            onMouseDown={onBgClick}
        >
            <div
                className={`absolute inset-0 ${transparentBg ? "hidden" : ""}`}
                style={{
                    backgroundColor: "hsla(var(--base-hue), var(--base-saturation), 3%, 0.5)",
                }}
            />
            <div
                ref={content}
                className={`${fixed ? "fixed" : "absolute"}`}
                style={{
                    zIndex,
                    maxHeight: maxHeight.current != null ? `${maxHeight.current}px` : undefined,
                }}
                onClick={onBgClick}>
                {children}
            </div>
        </div>
    )
}
