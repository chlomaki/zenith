import { montserratSemiBold } from "@/fonts";

interface AuthenticationFormProps {
    title: string;
    children: React.ReactNode;
    cta: () => void;
}

export default function AuthenticationForm({ title, children, cta }: AuthenticationFormProps) {
    return <div className="flex flex-col gap-y-4 px-6 py-4 rounded-md shadow-md bg-col-[9%]">
        <h1 className={`text-xl ${montserratSemiBold.className}`}>{title}</h1>
        {children}
        <button className="flex justify-center items-center rounded-md bg-acc-[40%] w-full h-12" type="button" onClick={cta}><p>{title}</p></button>
    </div>
}