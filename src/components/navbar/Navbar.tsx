"use client";

import "./navbar.scss";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useRef } from "react";
import { self } from "@/account";
import { popup } from "@/client";
import Modal from "@/components/Modal";
import LoginForm from "@/components/navbar/LoginForm";
import RegisterForm from "@/components/navbar/RegisterForm";
import { montserratSemiBold } from "@/fonts";

export default function Navbar() {
    const pathname = usePathname();
    const isHome = pathname === "/";

    const login = useRef<HTMLDivElement>(null);
    const register = useRef<HTMLDivElement>(null);

    function onLogin() {
        const modal = <Modal
            anchor={{ x: "left", y: "bottom" }}
            src={login}
            noOverlap={true}
            onOpen={() => {
                login.current!.style.color = "hsl(var(--base-hue), var(--base-saturation), 80%)";
            }}
            onClose={() => {
                if (component != null)
                    component.dispose();

                login.current!.style.color = "inherit";
            }}>
            <div className="mt-4">
                <LoginForm />
            </div>
        </Modal>;

        let component: { dispose: () => void };

        (async () => {
            component = await popup(modal);
        })();
    }

    function onRegister() {
        const modal = <Modal
            anchor={{ x: "left", y: "bottom" }}
            src={register}
            noOverlap={true}
            onOpen={() => {
                register.current!.style.color = "hsl(var(--base-hue), var(--base-saturation), 80%)";
            }}
            onClose={() => {
                if (component != null)
                    component.dispose();

                register.current!.style.color = "inherit";
            }}>
            <div className="mt-4">
                <RegisterForm />
            </div>
        </Modal>;

        let component: { dispose: () => void };

        (async () => {
            component = await popup(modal);
        })();
    }

    return (
        <div className={`pointer-events-none z-10 absolute inset-0 flex h-40 justify-center px-4 ${isHome ? "_home" : ""}`}>
            <div className="pointer-events-auto flex flex-row h-10 mt-2 justify-between items-center max-w-7xl w-full bg-col-[9%] rounded-md px-6 shadow-lg">
                <div className="flex items-center h-full">
                    <h1 className={`${montserratSemiBold.className}`}>
                        <Link href="/">Challenge List</Link>
                    </h1>
                    <span className="w-[3px] h-3/6 ml-4 bg-col-[17%] rounded-full"></span>
                    <Link href="/test" className="ml-4">Test</Link>
                </div>
                <div className="flex gap-4 items-center h-full txt-col-[40%]">
                    {self == null ? <>
                        <div
                            className="cursor-pointer"
                            onClick={onLogin}
                            ref={login}
                        >
                            <p>Login</p>
                        </div>
                        <div
                            className="cursor-pointer"
                            onClick={onRegister}
                            ref={register}
                        >
                            <p>Register</p>
                        </div>
                    </> : <>
                        <Link href={"/profiles/" + self.id}>{self.username}</Link>
                    </>}
                </div>
            </div>
        </div>
    );
}
