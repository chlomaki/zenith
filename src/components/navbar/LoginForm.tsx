"use client";

import { login } from "@/account";
import { TextField } from "@/components/fields/TextField";
import AuthenticationForm from "@/components/navbar/AuthenticationForm";
import { useEffect, useRef } from "react";

export default function LoginForm() {
    const username = useRef<HTMLInputElement>(null);
    const password = useRef<HTMLInputElement>(null);
    const errField = useRef<HTMLDivElement>(null);

    async function performLogin() {
        try {
            await login(username.current!.value, password.current!.value);
            window.location.reload();
        } catch (e: any) {
            errField.current!.textContent = e.message;
        }
    }

    useEffect(() => {
        username.current?.focus();
    }, [])

    return <AuthenticationForm title="Login" cta={performLogin}>
        <div className="flex flex-col gap-y-2">
            <TextField ref={username} type="text" placeholder="Username" />
            <TextField ref={password} type="password" placeholder="Password" />
            <div ref={errField} className="text-red-500"></div>
        </div>
    </AuthenticationForm>
}