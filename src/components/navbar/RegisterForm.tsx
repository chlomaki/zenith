"use client"

import { login } from "@/account";
import { api } from "@/api/api";
import { TextField } from "@/components/fields/TextField";
import AuthenticationForm from "@/components/navbar/AuthenticationForm";
import { useEffect, useRef } from "react";

export default function RegisterForm() {
    const username = useRef<HTMLInputElement>(null);
    const password = useRef<HTMLInputElement>(null);
    const errField = useRef<HTMLDivElement>(null);
    
    async function performRegistration() {
        errField.current!.textContent = "";
        
        const resp = await api.account.authorization.register({
            username: username.current!.value,
            password: password.current!.value
        });

        if (resp.ok) {
            await login(username.current!.value, password.current!.value);
            window.location.reload();
        } else {
            errField.current!.textContent = resp.val.message;
        }
    }

    useEffect(() => {
        username.current?.focus();
    }, [])
    
    return <AuthenticationForm title="Register" cta={performRegistration}>
        <div className="flex flex-col gap-y-2">
            <TextField ref={username} type="text" placeholder="Username" />
            <TextField ref={password} type="password" placeholder="Password" />
            <div ref={errField} className="text-red-500"></div>
        </div>
    </AuthenticationForm>
}