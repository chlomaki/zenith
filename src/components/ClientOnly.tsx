"use client"

import React, { ReactElement } from "react";
import { useHydrated } from "../useHydrated";

export default function ClientOnly({ children, fallback }: { children: React.ReactNode, fallback?: React.ReactNode }) {
    return useHydrated() ? <>{children}</> : <>{fallback}</>
}