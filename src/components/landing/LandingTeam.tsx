"use client";

import { interSemiBold } from "@/fonts";
import { Account } from "../../api/account";
import LandingTeamMember from "./LandingTeamMember";

interface LandingTeamProps {
    listTeam: Account[];
    developmentTeam: Account[];
}

export default function LandingTeam({
    listTeam,
    developmentTeam,
}: LandingTeamProps) {
    return (
        <div className="w-full flex justify-center items-center">
            <div className="mb-12 w-[70%] grid grid-cols-2 gap-x-4 gap-y-2">
                <div>
                    <p className={`${interSemiBold.className} mb-2`}>Challenge List Team</p>
                    <p className="txt-col-[60%] mb-4">
                        The Challenge List is managed by a fairly sized team of
                        players consisting of the following people:
                    </p>
                </div>
                <div>
                    <p className={`${interSemiBold.className} mb-2`}>Development Team</p>
                    <p className="txt-col-[60%] mb-4">
                        This instance of Pointercrate, and all related softwares
                        and technologies to the GD Challenge List are managed by
                        the following people:
                    </p>
                </div>
                <div className="grid grid-cols-2 gap-y-4 h-8">
                    {listTeam.map((account) => (
                        <LandingTeamMember key={account.id} account={account} />
                    ))}
                </div>
                <div className="grid grid-cols-2 gap-y-4 h-8">
                    {developmentTeam.map((account) => (
                        <LandingTeamMember key={account.id} account={account} />
                    ))}
                </div>
            </div>
        </div>
    );
}
