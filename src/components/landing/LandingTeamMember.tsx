import Image from "next/image";
import { Account } from "../../api/account";

interface LandingTeamMember {
    account: Account;
}

export default function LandingTeamMember({ account }: LandingTeamMember) {
    return (
        <div className="flex items-center gap-2">
            <Image
                src="/46827a6a0ee64a84bc4cdd5f5d0b4a80.jpg"
                alt="Staff Profile"
                width="64"
                height="64"
                // eslint-disable-next-line react/forbid-component-props
                className="object-cover w-8 h-8 rounded-full"
            />
            <p>{account.username}</p>
        </div>
    );
}
