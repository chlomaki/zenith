import { forwardRef } from "react";
import styles from "./textField.module.css";

export interface TextFieldProps {
    onInput?: (event: React.FormEvent<HTMLInputElement>) => void
    type: "text" | "password";
    placeholder?: string;
    minLength?: number;
    maxLength?: number;
}

export const TextField = forwardRef(function TextField(props: TextFieldProps, ref: React.ForwardedRef<HTMLInputElement>) {
    return <input ref={ref} className={styles.textField} {...props} />
})
