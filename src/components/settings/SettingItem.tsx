import { interSemiBold } from "@/fonts";

export function SettingItem({ label, description, children }: { label: string, description?: string | React.ReactNode, children: React.ReactNode }) {
    return <div className="grid grid-cols-2 gap-2">
        <div className="flex flex-col gap-2 justify-center">
            <div className={`${interSemiBold.className} txt-col-[80%]`}>{label}</div>
            <div className="txt-col-[40%]">
                {description}
            </div>
        </div>
        {children}
    </div>;
}