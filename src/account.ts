"use client";

import { reactive } from "@vue/reactivity";
import { Account as ApiAccount } from "@/api/account";
import { localStorage } from "@/localStorage";
import { api } from "./api/api";
import envOption from "./env";
import { assertOk } from "./util/Assert";
import { Logger } from "./util/Logger";

type Account = ApiAccount & { tokenId: string };

const logger = new Logger("account");

const accountData = localStorage.getItem("account");

export const self = accountData ? reactive(JSON.parse(accountData) as Account) : undefined;

export async function login(username: Account["username"], password: string, redirect?: string) {
    logger.debug(`logging in as ${username}`);
    
    const resp = await api.account.authorization.login({ username, password });
    assertOk(resp);

    logger.debug("OK");

    const account = resp.val.data.account as Account;
    account.tokenId = resp.val.data.id;

    localStorage.setItemAsJson("account", account);
    document.cookie = `session=${resp.val.data.token}; path=/; max-age=31536000`;

    if (redirect) {
        location.href = redirect;
    }
}

export async function logout() {
    if (!self) return;

    localStorage.removeItem("account");
    document.cookie = "session=; path=/; max-age=0";
}

export async function refreshAccount() {
    if (!self) return;

    const resp = await api.account.fetch.self();
    assertOk(resp);
    updateAccount(resp.val.data);
}

export function updateAccount(accountData: Partial<Account>) {
    if (!self) return;
    for (const [key, value] of Object.entries(accountData)) {
        (self as any)[key] = value;
    }

    localStorage.setItemAsJson("account", self);
}

if (envOption.isDebug)
    (window as any).$self = self;
