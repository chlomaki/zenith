import { z } from "zod";
import apiDefaultResponse from "@/api/apiDefaultResponse";
import { recordMinified } from "@/api/challenge";
import { createProcedure } from "@/api/router";

export const player = z.object({
    id: z.string(),
    username: z.string(),
    banned: z.boolean(),
    flags: z.number()
});

export type Player = z.infer<typeof player>;


export const playerProfile = z.object({
    id: z.string(),
    username: z.string(),
    banned: z.boolean(),
    flags: z.number(),
    completions: z.nullable(z.optional(z.array(recordMinified))),
    verifications: z.nullable(z.optional(z.array(recordMinified)))
});

export type PlayerProfile = z.infer<typeof playerProfile>;

export const playerRouter = {
    fetch: {
        paginate: createProcedure()
            .meta({ path: "/players", method: "GET" })
            .output(apiDefaultResponse(z.array(playerProfile))),
        byId: createProcedure()
            .meta({ path: "/players/:id", method: "GET" })
            .output(apiDefaultResponse(playerProfile)),
        profileById: createProcedure()
            .meta({ path: "/players/:id/profile", method: "GET" })
            .output(apiDefaultResponse(playerProfile)),
    }
};
