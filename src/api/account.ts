import { z } from "zod";
import apiDefaultResponse from "./apiDefaultResponse";
import { group } from "./group";
import { createProcedure } from "./router";

export const account = z.object({
    id: z.string(),
    username: z.string(),
    avatar_hash: z.string(),
    banner_hash: z.string(),
    created_at: z.number(),
    updated_at: z.optional(z.number()),
    country_code: z.nullable(z.optional(z.string())),
    subdivision_code: z.nullable(z.optional(z.string())),
    flags: z.number(),
    groups: z.array(group),
});

export type Account = z.infer<typeof account>;

export const session = z.object({
    id: z.string(),
    token: z.string(),
    created_at: z.number(),
    updated_at: z.optional(z.number()),
    account: account,
});

export type Session = z.infer<typeof session>;

export const publicSession = z.object({
    id: z.string(),
    created_at: z.number(),
    updated_at: z.optional(z.number()),
});

export type PublicSession = z.infer<typeof publicSession>;

export const accountRouter = {
    authorization: {
        register: createProcedure()
            .meta({ path: "/authorization/register", method: "POST" })
            .input(
                z.object({
                    username: z.string(),
                    password: z.string(),
                })
            )
            .output(apiDefaultResponse(account)),
        login: createProcedure()
            .meta({ path: "/authorization/login", method: "POST" })
            .input(
                z.object({
                    username: z.string(),
                    password: z.string(),
                })
            )
            .output(apiDefaultResponse(session)),
        // todo : logout event for the API?
        getSessions: createProcedure()
            .meta({ path: "/accounts/self/sessions", method: "GET" })
            .output(apiDefaultResponse(z.array(publicSession))),
    },
    fetch: {
        byId: createProcedure()
            .meta({ path: "/accounts/:id", method: "GET" })
            .output(apiDefaultResponse(account)),
        self: createProcedure()
            .meta({ path: "/accounts/self", method: "GET" })
            .output(apiDefaultResponse(account)),
    }
};
