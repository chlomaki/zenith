import { z } from "zod";
import apiDefaultResponse from "./apiDefaultResponse";
import { createProcedure } from "./router";

export const player = z.object({
    id: z.string(),
    username: z.string(),
    banned: z.boolean()
});

export type Player = z.infer<typeof player>;

export const challengeMinified = z.object({
    id: z.string(),
    name: z.string(),
    position: z.number(),
    video: z.string(),
    level_id: z.nullable(z.optional(z.string())),
    verifier: z.nullable(z.optional(player)),
    publisher: z.nullable(z.optional(player)),
});

export type ChallengeMinified = z.infer<typeof challengeMinified>;

export const recordMinified = z.object({
    id: z.string(),
    submitted_at: z.number(),
    updated_at: z.number(),
    video: z.string(),
    status: z.number(),
    type: z.number(),
    challenge: challengeMinified
});

export type RecordMinified = z.infer<typeof recordMinified>;

export const record = z.object({
    id: z.string(),
    submitted_at: z.number(), // todo : date
    updated_at: z.number(),
    video: z.string(),
    status: z.number(), // todo : enum
    type: z.number(), // todo : ^
    challenge: challengeMinified,
    player: player
});

export type Record = z.infer<typeof record>;

export const challenge = z.object({
    id: z.string(),
    name: z.string(),
    position: z.number(),
    video: z.string(),
    level_id: z.nullable(z.optional(z.string())),
    verifier: z.nullable(z.optional(player)),
    publisher: z.nullable(z.optional(player)),
    creators: z.nullable(z.optional(z.array(player))),
    records: z.nullable(z.optional(z.array(record)))
});

export type Challenge = z.infer<typeof challenge>;

export enum ChallengeType {
    Main = "main",
    Legacy = "legacy"
}

export const challengeRouter = {
    crud: {
        create: createProcedure()
            .meta({ path: "/challenges/", method: "POST" })
            .input(z.object({
                name: z.string(),
                position: z.number(),
                verifier: z.string(),
                creators: z.array(z.string()),
                publisher: z.string(),
                video: z.string(),
                fps: z.optional(z.string())
            }))
            .output(apiDefaultResponse(challenge)),
        patch: createProcedure()
            .meta({ path: "/challenges/:id", method: "PATCH" })
            .input(z.object({
                name: z.optional(z.string()),
                position: z.optional(z.string())
            }))
            .output(apiDefaultResponse(challenge))
    },
    fetch: {
        byId: createProcedure()
            .meta({ path: "/challenges/:id", method: "GET" })
            .output(apiDefaultResponse(challenge))
            .cache("force-cache"),
        list: createProcedure()
            .meta({ path: "/challenges/list", method: "GET" })
            .input(z.object({
                type: z.nativeEnum(ChallengeType)
            }))
            // todo
            .output(apiDefaultResponse(z.object({

            }))),
        position: createProcedure()
            .meta({ path: "/challenges/list/:position", method: "GET" })
            .output(apiDefaultResponse(challenge)),
        index: createProcedure()
            .meta({ path: "/challenges/", method: "GET" })
            // todo : pagination
            .output(apiDefaultResponse(z.array(challenge)))
    }
};
