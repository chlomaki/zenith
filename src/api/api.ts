import { playerRouter } from "@/api/player";
import { Logger } from "@/util/Logger";
import { accountRouter } from "./account";
import { challengeRouter } from "./challenge";
import { createClient } from "./client";

const logger = new Logger("api");

export function getBaseUrl() {
    if (process.env.NODE_ENV === "production") return "https://next.challengelist.gd";
    return `http://localhost:${process.env.API_PORT ?? 848}`;
}

createClient({
    baseUrl: getBaseUrl(),
    routes: [
        accountRouter,
        challengeRouter,
        playerRouter
    ],
    extensions: [
        {
            beforeRequest(req) {
                logger.info(`Requesting to ${req.finalPath}`);
            },
        }
    ]
});

export const api = {
    account: accountRouter,
    challenge: challengeRouter,
    player: playerRouter
};
