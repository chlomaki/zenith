import { Montserrat, Inter } from "next/font/google";

export const inter = Inter({ subsets: ["latin"] });
export const interSemiBold = Inter({ subsets: ["latin"], weight: "600" });

export const montserrat = Montserrat({ subsets: ["latin"] });
export const montserratSemiBold = Montserrat({ subsets: ["latin"], weight: "600" });