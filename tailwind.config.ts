import type { Config } from "tailwindcss";
import plugin from "tailwindcss/plugin";

const config: Config = {
    darkMode: ["class"],
    content: [
        "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    ],
    plugins: [
        require("tailwindcss-animate"),
        plugin(function ({ matchUtilities }) {
            matchUtilities({
                "bg-col": (value) => ({
                    "background-color": `hsl(var(--base-hue), var(--base-saturation), ${value})`,
                }),
                "bord-col": (value) => ({
                    "border-color": `hsl(var(--base-hue), var(--base-saturation), ${value})`,
                }),
                "txt-col": (value) => ({
                    color: `hsl(var(--base-hue), var(--base-saturation), ${value})`,
                }),
                "bg-acc": (value) => ({
                    "background-color": `hsl(var(--base-hue), var(--base-accent), ${value})`,
                }),
                "txt-acc": (value) => ({
                    color: `hsl(var(--base-hue), var(--base-accent), ${value})`,
                }),
            });
        }),
    ],
};
export default config;
