/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
            // for testing purposes...
            {
                protocol: "https",
                hostname: "cdn.discordapp.com",
            }
        ]
    }
};

export default nextConfig;
